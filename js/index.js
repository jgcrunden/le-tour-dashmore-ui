const board = document.querySelector(".board");
let lastUpdated = null;
let ashmores = null;

const generateAshmoreCard = (ashmore) => {
    let competitorCard = `
    <div class="competitor-card ${ashmore.ID}">
        <div class="team-header">
            <div>Team name</div>
            <div class="stat">Time Difference</div>
            <div>Jerseys</div>
        </div>
        <div class="ashmore">
            <div class="competitor-name">${ashmore.Name}</div>
            <div class="competitor-score">${ashmore.Time}</div>
            <div class="jerseys ${ashmore.ID}">
            </div>
        </div>
        <div class="bar-section">
            <div class="bar ${ashmore.ID}"></div>
        </div>
        <div class="icons">
            <i class="fas fa-plus ${ashmore.ID}"></i>
            <i class="fas fa-minus hidden ${ashmore.ID}"></i>
        </div>

        <div class="riders ${ashmore.ID} hidden">
        <div class="rider-header">
            <div>Rider name</div>
            <div class="stat">Time Difference</div>
            <div>Jerseys</div>
        </div>
        </div>

    </div>`
    return competitorCard;
}

const jerseyHtml = (colour) => {
    return `<div class="shirt-container">
        <i class="fas fa-tshirt fa-tshirt-${colour}"></i>
    </div>`;
}

const addJerseys = (id, jerseys) => {
    const jerseySection = document.querySelector(`.jerseys.${id}`);
    let jerseyEl = "";
    for(var i = 0; i < jerseys.length; i++) {
        if(jerseys[i] === "gc") {
            jerseyEl = jerseyHtml("yellow");
        } else if (jerseys[i] === "s") {
            jerseyEl = jerseyHtml("green");
        } else if (jerseys[i] === "c") {
            jerseyEl = jerseyHtml("red");
        } else if (jerseys[i] === "y") {
            jerseyEl = jerseyHtml("white");
        }
        jerseySection.insertAdjacentHTML('beforeend', jerseyEl);
    }
    jerseySection.se
}

const generateRiderCard = (rider) => {
    return `
        <div class="rider ${rider.ID}">
            <div class="competitor-name">${rider.Name}</div>
            <div class="competitor-score">${rider.Time}</div>
            <div class="jerseys ${rider.ID}">
            </div>
        </div>
        <div class="bar-section">
            <div class="bar ${rider.ID}"></div>
        </div>
    `
}

const DNFTag = `<div>DNF</div>`

const addRiders = (ashmore, riders, colour) => {
    const ashmoreCard = document.querySelector(`.riders.${ashmore}`)

    for(var i = 0; i < riders.length; i++) {
;       const riderHtml = generateRiderCard(riders[i]);
        ashmoreCard.insertAdjacentHTML('beforeend', riderHtml);
        if(riders[i].DNF) {
            const riderCard = document.querySelector(`.rider.${riders[i].ID}`);
            riderCard.insertAdjacentHTML('beforeend', DNFTag);
        }
        if(riders[i].Jerseys.length > 0) {
            addJerseys(riders[i].ID, riders[i].Jerseys)
        }
        assignBarWidth(riders[i]);
        colourBar(riders[i].ID, colour)
    }
}

const url = "https://be3zkflp95.execute-api.eu-west-1.amazonaws.com/prod/results"
// const url = "http://localhost:3001/tour"

const request = new XMLHttpRequest();

request.open("GET", url);
request.onload = (() => {
    const data = JSON.parse(request.response);
    let body = JSON.parse(data.body);
	lastUpdated = body.LastUpdated
	ashmores = body.Results
    displayResults()
})

request.send();

const assignBarWidth = (person) => {
    const bar = document.querySelector(`.bar.${person.ID}`);
    bar.style.width = person.Percentage + '%';
}

const colourBar = (name, colour) => {
    const bar = document.querySelector(`.bar.${name}`);
    bar.style.backgroundColor = colour;
}

const toggleDisplay = (ashmore) => {
    const riders = document.querySelector(`.riders.${ashmore}`);
    const plus = document.querySelector(`.fa-plus.${ashmore}`);
    const minus = document.querySelector(`.fa-minus.${ashmore}`);

    if(riders.classList.contains('hidden')) {
        riders.classList.remove('hidden');
        plus.classList.add('hidden');
        plus.classList.remove('icon-style');
        minus.classList.remove('hidden')
        minus.classList.add('icon-style');

    } else {
        riders.classList.add('hidden')
        plus.classList.remove('hidden');
        plus.classList.add('icon-style');
        minus.classList.add('hidden')
        minus.classList.remove('icon-style');
    }
}

const addAshmoreClickListener = (ashmore) => {
    const plus = document.querySelector(`.fa-plus.${ashmore}`);
    const minus = document.querySelector(`.fa-minus.${ashmore}`);
    plus.addEventListener('click', () => {
        toggleDisplay(ashmore);
    })

    minus.addEventListener('click', () => {
        toggleDisplay(ashmore);
    })
}

const removeLoadingMessage = () => {
    const loadingSection = document.querySelector(".loading-section");
    loadingSection.remove();
}

const displayResults = () => {
    removeLoadingMessage();
    for (let i = 0; i < ashmores.length; i++) {
        const cardHtml = generateAshmoreCard(ashmores[i]);
        board.insertAdjacentHTML('beforeend', cardHtml);
        if(ashmores[i].Jerseys.length > 0) {
            addJerseys(ashmores[i].ID, ashmores[i].Jerseys)
        }
        assignBarWidth(ashmores[i]);
        colourBar(ashmores[i].ID, ashmores[i].GraphColour);
        addRiders(ashmores[i].ID, ashmores[i].Riders, ashmores[i].GraphColour);
        addAshmoreClickListener(ashmores[i].ID)
    }
}

const hamburger = document.querySelector('.hamburger');
const menu = document.querySelector('.menu');
const cross = document.querySelector('.cross');
hamburger.addEventListener('click', () => {
    menu.classList.remove('hidden');
    hamburger.classList.add('hidden');
    cross.classList.remove('hidden');
})

const closeMenu = () => {
    menu.classList.add('hidden');
    hamburger.classList.remove('hidden');
    cross.classList.add('hidden');
}

cross.addEventListener('click', closeMenu)

const optionsTeam = document.querySelectorAll('.option-team');

const changeTeamStats = (stat) => {
    const competitorScore = document.querySelectorAll('.ashmore .competitor-score');
    for(var i = 0; i < ashmores.length; i++) {
        competitorScore[i].innerHTML = ashmores[i][stat];
    }
}

const changeTeamMenuCheck = (menuItem) => {
    const check = document.querySelectorAll('.option-team .option-check .fa-check-circle')
    for(var i = 0; i < optionsTeam.length; i++) {
        if(optionsTeam[i].classList[2] !== menuItem)
            check[i].classList.add('hidden');
        else
            check[i].classList.remove('hidden');
    }
}

const changeRiderStats = (stat) => {
    for(var i = 0; i < ashmores.length; i++) {
        const competitorScore = document.querySelectorAll(`.riders.${ashmores[i].ID} .rider .competitor-score`)
        for(var j = 0; j < ashmores[i].Riders.length; j++) {
            if(ashmores[i].Riders[j][stat] !== 0 && ashmores[i].Riders[j][stat] !== "")
                competitorScore[j].innerHTML = ashmores[i].Riders[j][stat]
            else
                competitorScore[j].innerHTML = "N/A"
        }
            
    }
}

const changeTeamHeader = (value) => {
    const el = document.querySelectorAll('.team-header .stat');
    el.forEach(e => {
        e.innerHTML = value;
    })
}

const changeRiderHeader = (value) => {
    const el = document.querySelectorAll('.rider-header .stat');
    el.forEach(e => {
        e.innerHTML = value;
    })
}

optionsTeam.forEach(e => {
    e.addEventListener('click', () => {
        const stat = e.classList[2].split('-')[2];
        changeTeamStats(stat)
        changeTeamHeader(e.children[1].innerHTML)
        changeTeamMenuCheck(e.classList[2])
        closeMenu();
    })
})

const optionsRider = document.querySelectorAll('.option-rider');

const changeRiderMenuCheck = (menuItem) => {
    const check = document.querySelectorAll('.option-rider .option-check .fa-check-circle')
    for(var i = 0; i < optionsRider.length; i++) {
        if(optionsRider[i].classList[2] !== menuItem)
            check[i].classList.add('hidden');
        else
            check[i].classList.remove('hidden');
    }
}

optionsRider.forEach(e => {
    e.addEventListener('click', () => {
        const stat = e.classList[2].split('-')[2];
        changeRiderStats(stat)
        changeRiderHeader(e.children[1].innerHTML)
        changeRiderMenuCheck(e.classList[2])
        closeMenu();
    })
})
